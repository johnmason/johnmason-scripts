import argparse
import re
from pathlib import Path
import shutil


parser = argparse.ArgumentParser(
                    prog='chunk.py',
                    description='Chunks up a directory of files based on target size',
                    )

parser.add_argument('path', help='Path to directory to chunk')
parser.add_argument('out', help='Path to directory to write chunks')
parser.add_argument('-s', '--size_gb', required=True, type=lambda x: float(x), help='Target size in GB for each chunk (approximately)')
parser.add_argument('-i', '--ignore', nargs='+', help='File basename patterns to ignore')
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('-f', '--force', action='store_true')

def info(*s, prefix="> "):
    print(f"{prefix}", *s)

def fail(*s):
    print()
    info(*s, prefix="!!!")
    print()
    exit()

args = parser.parse_args()
info(args)


def should_ignore(s, ignore_patterns):
    for ignore in ignore_patterns:
        pattern = re.compile(f"^{ignore}$")
        if pattern.match(s):
            return True

    return False


def enumerate_dir_recursively(dir, ignore_patterns):
    path = Path(dir)
    if not path.is_dir():
        fail(f"Is not a directory: {dir}")

    for p in sorted(path.iterdir()):
        if should_ignore(p.name, ignore_patterns):
            info(f"IGNORE: {p}", prefix="   - ")
            continue

        if p.is_dir():
            for rp in enumerate_dir_recursively(p, ignore_patterns):
                yield rp
        else:
            yield p
        

if Path(args.out).is_dir():
    if not args.force:
        fail('Output directory already exists. Use --force if you want it to be deleted and replaced with output.')
    else:
        info(f"Removing existing directory", args.out)
        shutil.rmtree(args.out)	


info("Chunking", args.path)
current_batch = 100
batch_bytes = 0
info("Starting batch:", current_batch)


for p in enumerate_dir_recursively(args.path, args.ignore):
    relative_dir = p.parent.relative_to(args.path)
    out_dir = Path(args.out) / f"{current_batch}" / relative_dir
    out_dir.mkdir(parents=True, exist_ok=True)

    out_file = out_dir / p.name
    shutil.copy(str(p), str(out_file))
    info(out_file, prefix="   - ")

    batch_bytes += out_file.stat().st_size
    if batch_bytes >= (args.size_gb * 1e9):
        current_batch += 1
        batch_bytes = 0
        info("Starting next batch:", current_batch)


info("done.")







