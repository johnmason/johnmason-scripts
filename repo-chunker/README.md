# Repo Chunker
## Usage

```
usage: chunk.py [-h] -s SIZE_GB [-i IGNORE [IGNORE ...]] [-v] [-f] path out

Chunks up a directory of files based on target size

positional arguments:
  path                  Path to directory to chunk
  out                   Path to directory to write chunks

optional arguments:
  -h, --help            show this help message and exit
  -s SIZE_GB, --size_gb SIZE_GB
                        Target size in GB for each chunk (approximately)
  -i IGNORE [IGNORE ...], --ignore IGNORE [IGNORE ...]
                        File basename patterns to ignore
  -v, --verbose
  -f, --force
```

### Example

```
python3 chunk.py -f -i .git tmp '.*zoekt' -s 0.25 ~/gitlab-development-kit/gitlab /tmp/foo
```