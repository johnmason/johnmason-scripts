#!/usr/bin/env bash

set -e

declare -a arr=("0.05" "0.15" "0.25" "0.5" "1" "1.5" "2")

for i in "${arr[@]}"
do
    output="/tmp/chunked-gitlab/$i"
    python3 chunk.py -f -i .git tmp '.*zoekt' -s "$i" ~/gitlab-development-kit/gitlab "$output"
    echo "Compressing $output ..."
    (
      cd /tmp/chunked-gitlab
      tar -czf "${i}.tar.gz" "$i"
    )
    echo "Cleaning up uncompressed directory: $output"
    rm -rf "$output"
done

