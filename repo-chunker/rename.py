import argparse
import re
from pathlib import Path
import shutil


parser = argparse.ArgumentParser(
                    prog='rename.py',
                    description='Renames files',
                    )

parser.add_argument('path', help='Path to directory to chunk')

def info(*s, prefix="> "):
    print(f"{prefix}", *s)


args = parser.parse_args()
info(args)

path = Path(args.path)

for d in path.iterdir():
    for p in sorted(d.glob("*.zoekt")):
        n = p.name.removeprefix("batch-").removeprefix("0").removeprefix("0")
        prefix, suffix = n.split("_")

        identifier = int(prefix) + 1

        new_name = d / f"{identifier}_{suffix}"

        print(new_name)
        p.rename(new_name)

