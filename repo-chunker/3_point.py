def equation_plane(x1, y1, z1, x2, y2, z2, x3, y3, z3): 
    a1 = x2 - x1
    b1 = y2 - y1
    c1 = z2 - z1
    a2 = x3 - x1
    b2 = y3 - y1
    c2 = z3 - z1
    a = b1 * c2 - b2 * c1
    b = a2 * c1 - a1 * c2
    c = a1 * b2 - b1 * a2
    d = (- a * x1 - b * y1 - c * z1)
    print("equation of plane is ", end="")
    print(a, "x + ",end="")
    print(b, "y + ",end="")
    print(c, "z + ",end="")
    print(d, "= 0.")

    print("A:", round(a, 4))
    print("B:", round(b, 4))
    print("C:", round(c, 4))
    print("D:", round(d, 4))

# z is total required memory
# x is total number of shards
# y is total disk usage 

x1, y1,z1 = 28, 2, 0.3106
x2,y2,z2 = 13, 2, 0.3699
x3,y3,z3 =  7, 0.389, 0.5295

equation_plane(x1, y1, z1, x2, y2, z2, x3, y3, z3)
